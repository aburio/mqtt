# Plateforme de Prototypage
## Partie 2 : Communiquer avec le Cloud

### Besoin d'aide ?
**Posez toutes vos questions sur le Gitter du cours :  [![Gitter](https://badges.gitter.im/PP-ECE/community.svg)](https://gitter.im/PP-ECE/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)**

### Prérequis
1. Avoir un binôme
2. Créer un compte Gitlab
3. Installer Git ([Windows](https://gitforwindows.org/), [Ubuntu](https://help.ubuntu.com/lts/serverguide/git.html), ou [OSX](https://git-scm.com/download/mac))
4. Installer [VS Code](https://code.visualstudio.com/)
5. Ajouter l'extension [PlatformIO IDE](https://platformio.org/) à VS Code

### Objectif
Votre objectif lors de ce TP est de mettre en oeuvre et structurer la connectivité d'un objet connecté avec le service Cloud AWS IoT core.

Afin de vérifier votre compréhension du fonctionnement de l'environnement de développement, **vous devrez mettre à jour votre `git repository` sur gitlab**.
Vous devrez donc `commit` vos modifications régulièrements. Chaque exercice, devra faire l'objet d'une `branch` distincte sur votre `repository`. Pour indiquer qu'un exercice est terminé, vous devrez tagguer le `commit` corrrespondant avec le formalisme suivant : **ex-N** ou N correspond au numéro de l'exercice.

**Avant d'implémenter une consigne bonus, terminez tous les exercices :)**

### Evaluation
L'évaluation de votre travail sera réalisée sur les critères suivants :
- Exercices terminées
- Respect des consignes de nommage des `branch` et `tag` git
- Contenu et description des `commit`
- Qualité du code (structure, lisibilité, commentaires utiles si et seulement si nécessaire...)
- Temps nécessaire pour réaliser les exercices

### Quelques resources
- [Documentation Heltec](https://heltec.org/project/wifi-kit-32/)
- [Reference framework Arduino](https://www.arduino.cc/reference/en/)
- [Reference framework ESP-IDF](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html)
- [Reference librairie u8g2](https://github.com/olikraus/u8g2/wiki/u8g2reference) : librairie utilisé pour l'ecran OLED
- [Reference librairie Pubsubclient](https://github.com/knolleary/pubsubclient) : librairie utilisé pour MQTT
- [Reference librairie ArduinoJson](https://arduinojson.org/) : librairie utilisé pour formatter les messages MQTT
- [Librairies framework Arduino ESP32](https://github.com/espressif/arduino-esp32/tree/master/libraries) : liste des librairies disponible dans le framework Arduino accompagnée nombreux exemples d'utilisation
- [Diagramme en Markdown](https://docs.gitlab.com/ee/user/markdown.html#diagrams-and-flowcharts) : une solution simple pour faire une machine à état

# Exercice 1 - Publier & Recevoir des informations via AWS IoT (Protocol MQTT) :

### Objectif
Afin de comprendre le fonctionnement du protocol MQTT et du service Cloud AWS IoT core, nous vous proposons la création d'une petite application pour envoyer et recevoir des message via le Cloud.

Votre code devra :
1. Lorsque l'application démarre, se connecter à un Wifi puis à AWS IoT core
2. Lorsqu'un utilisateur appui plus de 3 secondes sur le bouton :
    - Publier un message au format JSON dans le topic `ecepp/AWS/Inbox`.
    - Le format du message devra être :
    ```
    { 
      "message" : "Bonjour",
      "from": "Bxx"
    }
    ``` 
    **Remplacer les xx par votre numéro de binôme**
3. Lorsqu'un message est publié dans le topic `ecepp/Devices/Inbox` l'afficher sur l'écran oled

### Instructions
1. Cloner le projet
2. Ouvrir le projet avec PlatformIO
2. **Build** le projet avec PlatformIO
3. **Upload & Monitor** le projet sur la carte Heltec Wifi kit 32
4. Etudier le fonctionnement du code fourni
5. Dessiner le diagramme de la machine à état correspondant à l'application demandé.
6. Créer une `branch` Exercice-1
7. Sauvegarder le diagramme au format png, jpg ou [md](https://docs.gitlab.com/ee/user/markdown.html) sous le nom `state_machine` à la racine de votre repository
8. Modifier le code pour compléter l'exercice et `commit` régulièrement
9. Tagguer le `commit` à corriger

# Exercice 2 - Structure la connectivité d'un objet :

### Objectif
Structurer la partie connectivité de votre application en modules fonctionnels comme étudié dans le TP `PokeAMole`.

Votre code devra :
1. Lorsque l'application démarre, se connecter à un Wifi puis à AWS IoT core et afficher l'état de connectivité sur l'ecran oled
2. Lorsque l'application est connectée et prête à être utilisée allumer la led et afficher le message `Ready` sur l'écran oled
3. Lorsqu'un utilisateur appui plus de 3 secondes sur le bouton :
    - Publier un message au format JSON dans le topic `ecepp/Devices/Inbox`.
    - Le format du message devra être :
    ```
    { 
      "message" : "Bonjour",
      "from" : "Bxx",
      "to" : "Bxx"
    }
    ```
    **Remplacer les xx par votre numéro de binôme ainsi que le numéro du binôme destinataire**
4. Lorsqu'un utilisateur appui 2 fois de suite sur le bouton (double clic) :
   - Publier un message au format JSON dans le topic `ecepp/Devices/Bxx`. (remplacer xx par votre numéro de binôme)
   - Le format du message devra être :
    ```
    { 
      "MAC Address" : "...",
      "SDK Version" : "...",
      "Firmware Size" : "..."
    }
    ```
    **Remplacer les ... par les informations correspondantes ((cf. Github arduino-esp32)[https://github.com/espressif/arduino-esp32/blob/master/cores/esp32/Esp.h])**
5. Lorsqu'un message est publié dans le topic `ecepp/Devices/Inbox`, vérifier si vous êtes le destinataire et l'afficher sur l'écran oled

**Bonus** : Créer un serveur HTTP local (cf. TP WifiAndOTA) avec votre ESP32 pour permettre à l'utilisateur de personnaliser le message et son destinataire

### Instructions
1. Dessiner le diagramme de la machine à état correspondant à l'application demandé.
2. Récupérer la `branch` Exercice-2 (checkout)
3. Sauvegarder le diagramme au format png, jpg ou [md](https://docs.gitlab.com/ee/user/markdown.html) sous le nom `state_machine` à la racine de votre repository
3. Modifier le code pour compléter l'exercice et `commit` à convenance
4. Tagguer le `commit` à corriger
