#include "aws.h"
#include "conf.h"

#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

#include "Button/button.h"
#include "Led/led.h"
#include "Oled/oled.h"

WiFiClientSecure secureClient = WiFiClientSecure();
PubSubClient mqttClient(secureClient);

void mqtt_inbox(String topic, byte* payload, unsigned int length);

void wifi_init()
{
  Serial.print("Connecting to ");
  Serial.println(wifi_ssid);

  WiFi.begin(wifi_ssid, wifi_pwd);

  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }

  Serial.print("Wifi Connected to ");
  Serial.println("IP " + WiFi.localIP().toString());
  oled_print("Wifi Connected");
  oled_print("IP " + WiFi.localIP().toString());
  oled_display();
}

void mqtt_init()
{
  mqttClient.setServer(AWS_END_POINT, 8883);
  secureClient.setCACert(AWS_CERT_CA);
  secureClient.setCertificate(AWS_CERT_CRT);
  secureClient.setPrivateKey(AWS_CERT_PRIVATE);

  Serial.println("Connecting to MQTT....");
  mqttClient.connect(DEVICE_NAME);

  while (!mqttClient.connected())
  {
    Serial.println("Connecting to MQTT....Retry");
    mqttClient.connect(DEVICE_NAME);
    delay(5000);
  }

  Serial.println("MQTT Connected");
  oled_print("MQTT Connected");
  oled_display();

  mqttClient.subscribe("ecepp/Inbox");
  mqttClient.setCallback(mqtt_inbox);
}

void mqtt_process()
{
  if (!mqttClient.connected()) {
    mqtt_init();
  }
  mqttClient.loop();
}

void mqtt_inbox(String topic, byte* payload, unsigned int length)
{
  if (length > 0) {
    char* message = new char[length];
    memcpy(message, payload, length);
    message[length] = 0;

    Serial.println("Message received from topic " + topic + " payload " + String(message));

    oled_clear();
    oled_print("Message received");
    oled_print(message);
    oled_display();
  }
}

void setup()
{
  Serial.begin(115200);

  button_init(BUTTON_GPIO);
  led_init(LED_GPIO);
  oled_init(SSD1306_RESET_GPIO, SSD1306_SCL_GPIO, SSD1306_SDA_GPIO);

  wifi_init();
  mqtt_init();
}

void loop()
{
  button_process();
  mqtt_process();
}

void button_long_press_cb()
{
  String message = "Bonjour";
  String from = "Bxx";

  Serial.print("message : ");
  Serial.println(message);
  Serial.print("from : ");
  Serial.println(from);

  StaticJsonDocument<128> jsonDoc;
  JsonObject eventDoc = jsonDoc.createNestedObject("event");
  eventDoc["message"] = message;
  eventDoc["from"] = from;

  char jsonBuffer[128];

  serializeJson(eventDoc, jsonBuffer);
  mqttClient.publish("ecepp", jsonBuffer);
}